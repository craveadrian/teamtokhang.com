<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="description" content="">
  	<meta name="author" content="">
    <?php wp_head();?>
    <title></title>
  </head>
  <body>
    <header>
      <div class="top-nav">
        <nav>
            <a href="#">HOME</a>
            <?php wp_list_pages( '&title_li=' ); ?>
        </nav>
      </div>
    </header>
